package com.uxs.orange.restaurant.dtos;

public class OrderDTO {

	private Long id;
	private double totalPrice;
	private double totalTaxes;
	private boolean discountApplied;
	private boolean payed;

	public Long getId() {
		return id;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public double getTotalTaxes() {
		return totalTaxes;
	}

	public boolean isDiscountApplied() {
		return discountApplied;
	}

	public boolean isPayed() {
		return payed;
	}

	public void setDiscountApplied(boolean discountApplied) {
		this.discountApplied = discountApplied;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setPayed(boolean payed) {
		this.payed = payed;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public void setTotalTaxes(double totalTaxes) {
		this.totalTaxes = totalTaxes;
	}

}
