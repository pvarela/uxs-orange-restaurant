package com.uxs.orange.restaurant.dtos;

import com.uxs.orange.restaurant.enums.MealCategory;

public class MealDTO {

	private Long id;
	private String name;
	private MealCategory category;
	private double price;
	private double taxes;
	private Integer quantity;

	public MealCategory getCategory() {
		return category;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public double getPrice() {
		return price;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public double getTaxes() {
		return taxes;
	}

	public double getTotalPrice() {
		return this.getPrice() + this.getPrice() * this.getTaxes();
	}

	public void setCategory(MealCategory category) {
		this.category = category;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public void setTaxes(double taxes) {
		this.taxes = taxes;
	}

}
