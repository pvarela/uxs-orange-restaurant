package com.uxs.orange.restaurant.dtos;

public class OrderDetailDTO {

	private long mealId;
	private Integer quantity;

	public long getMealId() {
		return mealId;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setMealId(long mealId) {
		this.mealId = mealId;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

}
