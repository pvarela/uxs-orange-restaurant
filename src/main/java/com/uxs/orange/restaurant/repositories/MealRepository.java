package com.uxs.orange.restaurant.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.uxs.orange.restaurant.entities.Meal;
import com.uxs.orange.restaurant.enums.MealCategory;

@Repository
public interface MealRepository extends JpaRepository<Meal, Long> {

	public List<Meal> findAllByCategory(MealCategory category);

	public List<Meal> findAllByQuantityEquals(Integer quantity);

	public List<Meal> findAllByQuantityGreaterThan(Integer quantity);
}
