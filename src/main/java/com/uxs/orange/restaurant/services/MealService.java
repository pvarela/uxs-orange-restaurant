package com.uxs.orange.restaurant.services;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uxs.orange.restaurant.dtos.MealDTO;
import com.uxs.orange.restaurant.entities.Meal;
import com.uxs.orange.restaurant.enums.MealCategory;
import com.uxs.orange.restaurant.repositories.MealRepository;

import ma.glasnost.orika.MapperFacade;

@Service
public class MealService {

	private static Logger logger = LogManager.getLogger(MealService.class);

	@Autowired
	private MealRepository mealRepository;

	@Autowired
	private MapperFacade orika;

	public MealDTO createMeal(MealDTO mealDto) {

		Meal meal = orika.map(mealDto, Meal.class);
		meal = mealRepository.save(meal);
		return orika.map(meal, MealDTO.class);

	}

	public Meal discountQuantity(Long id, int quantity) {
		Optional<Meal> optionalMeal = mealRepository.findById(id);

		if (!optionalMeal.isPresent()) {
			return null;
		}

		Meal meal = optionalMeal.get();
		if (meal.getQuantity() < quantity) {
			logger.warn("You are asking more quantity of meal than stock has");
			return null;
		}

		Integer totalQuantity = meal.getQuantity() - quantity;
		meal.setQuantity(totalQuantity);

		return mealRepository.save(meal);

	}

	public List<MealDTO> findAll() {
		return orika.mapAsList(mealRepository.findAll(), MealDTO.class);
	}

	public List<MealDTO> findAllAvailable() {
		return orika.mapAsList(mealRepository.findAllByQuantityGreaterThan(0), MealDTO.class);
	}

	public List<MealDTO> findAllByCategory(String category) {
		MealCategory mealCategory = MealCategory.valueOf(category);
		return orika.mapAsList(mealRepository.findAllByCategory(mealCategory), MealDTO.class);
	}

	public List<Meal> findAllUnvailable() {
		return mealRepository.findAllByQuantityEquals(0);
	}

	public Meal findById(Long id) {
		Optional<Meal> meal = mealRepository.findById(id);
		if (!meal.isPresent()) {
			return null;
		}
		return meal.get();
	}

	public Meal refill(Meal meal, Integer quantity) {
		meal.setQuantity(quantity);
		return mealRepository.save(meal);
	}

}
