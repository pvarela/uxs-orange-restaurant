package com.uxs.orange.restaurant.services;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uxs.orange.restaurant.dtos.OrderDTO;
import com.uxs.orange.restaurant.dtos.OrderDetailDTO;
import com.uxs.orange.restaurant.entities.Meal;
import com.uxs.orange.restaurant.entities.Order;
import com.uxs.orange.restaurant.entities.OrderDetail;
import com.uxs.orange.restaurant.helpers.MealDiscountHelper;
import com.uxs.orange.restaurant.repositories.OrderRepository;

import ma.glasnost.orika.MapperFacade;

@Service
public class OrderService {

	private static Logger logger = LogManager.getLogger(OrderService.class);

	@Autowired
	private MapperFacade orika;

	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	private MealService mealService;

	@Autowired
	private MealDiscountHelper mealDiscountHelper;

	public OrderDTO createOrder(List<OrderDetailDTO> orderDetailsDto) {

		List<OrderDetail> orderDetails = orika.mapAsList(orderDetailsDto, OrderDetail.class);

		Meal meal = null;
		for (OrderDetail orderDetail : orderDetails) {
			meal = mealService.discountQuantity(orderDetail.getMealId(), orderDetail.getQuantity());
			if (meal == null) {
				logger.warn("The order has not been created because we have run out of a meal");
				return null;
			}
		}

		Order order = new Order();
		order.setPayed(false);
		order.setOrderDetail(orderDetails);
		generateBill(order);

		return orika.map(orderRepository.save(order), OrderDTO.class);
	}

	public List<OrderDTO> findAll() {
		return orika.mapAsList(orderRepository.findAll(), OrderDTO.class);
	}

	public OrderDTO payOrder(Long id) {

		Optional<Order> optionalOrder = orderRepository.findById(id);
		if (!optionalOrder.isPresent()) {
			logger.error("The order with id " + id + " has not been modified");
			return null;
		}

		Order order = optionalOrder.get();
		order.setPayed(true);
		logger.info("The order with id " + id + " has been paid");
		return orika.map(orderRepository.save(order), OrderDTO.class);
	}

	private void generateBill(Order order) {

		double taxes = 0;
		double discountApplied = 0;
		double amount = 0;

		Meal meal = null;

		for (OrderDetail orderDetail : order.getOrderDetail()) {
			meal = mealService.findById(orderDetail.getMealId());
			if (meal != null) {
				discountApplied += (mealDiscountHelper.applyMealDiscounts(meal)) * orderDetail.getQuantity();
				amount += meal.getTotalPrice() * orderDetail.getQuantity();
				taxes += (meal.getPrice() * meal.getTaxes()) * orderDetail.getQuantity();
			}

		}

		if (discountApplied > 0) {
			order.setDiscountApplied(true);
		}

		order.setTotalTaxes(taxes);
		order.setTotalPrice(amount - discountApplied);

	}
}
