package com.uxs.orange.restaurant.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uxs.orange.restaurant.entities.Order;
import com.uxs.orange.restaurant.repositories.OrderRepository;

/**
 * Service for specific functions related with the restaurant management
 */
@Service
public class RestaurantService {

	@Autowired
	private OrderRepository orderRepository;

	public double getIncoming() {
		double totalRevenue = 0;
		List<Order> orders = orderRepository.findAll();

		for (Order order : orders) {
			totalRevenue += order.getTotalPrice() - order.getTotalTaxes();
		}

		return totalRevenue;
	}

	public double getOffersRatio() {
		double counter = 0;

		List<Order> orders = orderRepository.findAll();

		for (Order order : orders) {
			if (order.isDiscountApplied()) {
				counter++;
			}
		}

		return counter / orders.size();
	}

	public double getTotalTaxes() {
		double totalTaxes = 0;
		List<Order> orders = orderRepository.findAll();

		for (Order order : orders) {
			totalTaxes += order.getTotalTaxes();
		}

		return totalTaxes;
	}
}
