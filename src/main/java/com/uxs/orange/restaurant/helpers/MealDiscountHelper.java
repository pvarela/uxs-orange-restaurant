package com.uxs.orange.restaurant.helpers;

import java.util.Calendar;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.uxs.orange.restaurant.entities.Meal;

@Component
public class MealDiscountHelper {

	@Value("${offers.drinks.monday.day}")
	private int dayDrinkOffer;

	@Value("${offers.drinks.monday.discount}")
	private double dayDrinkDiscount;

	@Value("${offers.mainCourse.day}")
	private int dayMainCourseOffer;

	@Value("${offers.mainCourse.discount}")
	private double dayMainCourseDiscount;

	public double applyMealDiscounts(Meal meal) {
		double totalDiscountApplied = drinksOffer(meal);
		totalDiscountApplied += mainCourseOffer(meal);
		return totalDiscountApplied;
	}

	public double drinksOffer(Meal meal) {
		Calendar myDate = Calendar.getInstance();
		int dow = myDate.get(Calendar.DAY_OF_WEEK);
		if (dow == getDayDrinkOffer()) {
			double discountApplied = meal.getPrice() * getDayDrinkDiscount();
			meal.setPrice(meal.getPrice() - discountApplied);
			return discountApplied;
		}
		return 0;
	}

	public double getDayDrinkDiscount() {
		return dayDrinkDiscount;
	}

	public int getDayDrinkOffer() {
		return dayDrinkOffer;
	}

	public double getDayMainCourseDiscount() {
		return dayMainCourseDiscount;
	}

	public int getDayMainCourseOffer() {
		return dayMainCourseOffer;
	}

	public double mainCourseOffer(Meal meal) {
		Calendar myDate = Calendar.getInstance();
		int dow = myDate.get(Calendar.DAY_OF_WEEK);
		if (dow == getDayMainCourseOffer()) {
			double discountApplied = meal.getPrice() * getDayMainCourseDiscount();
			meal.setPrice(meal.getPrice() - discountApplied);
			return discountApplied;
		}
		return 0;
	}

}
