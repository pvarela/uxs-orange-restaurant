package com.uxs.orange.restaurant.enums;

public enum MealCategory {
	APPETIZER, MAIN_COURSE, SECOND_COURSE, DRINK, DESSERT;

}
