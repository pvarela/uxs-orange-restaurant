package com.uxs.orange.restaurant.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "ORDERS_MEALS")
public class OrderDetail {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Long id;

	private long mealId;

	@Column
	@NotNull
	private Integer quantity;

	public Long getId() {
		return id;
	}

	public long getMealId() {
		return mealId;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setMealId(long mealId) {
		this.mealId = mealId;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

}
