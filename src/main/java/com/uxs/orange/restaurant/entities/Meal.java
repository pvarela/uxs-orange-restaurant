package com.uxs.orange.restaurant.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.uxs.orange.restaurant.enums.MealCategory;

@Entity
@Table(name = "MEALS")
public class Meal {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column
	@NotNull
	private String name;

	@Column
	@NotNull
	private double price;

	@Column
	@Min(0)
	@Max(1)
	private double taxes;

	@Column
	@NotNull
	private Integer quantity;

	@Column
	@NotNull
	private MealCategory category;

	public MealCategory getCategory() {
		return category;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public double getPrice() {
		return price;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public double getTaxes() {
		return taxes;
	}

	public double getTotalPrice() {
		return this.getPrice() + this.getPrice() * this.getTaxes();
	}

	public void setCategory(MealCategory category) {
		this.category = category;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public void setTaxes(double taxes) {
		this.taxes = taxes;
	}

}
