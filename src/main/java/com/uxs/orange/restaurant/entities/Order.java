package com.uxs.orange.restaurant.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Min;

@Entity
@Table(name = "ORDERS")
public class Order {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Long id;

	@Column
	@Min(0)
	private double totalPrice;

	@Column
	private double totalTaxes;

	@Column
	private boolean discountApplied;

	@Column
	private boolean payed;

	@OneToMany(cascade = CascadeType.ALL)
	private List<OrderDetail> orderDetail;

	public Long getId() {
		return id;
	}

	public List<OrderDetail> getOrderDetail() {
		return orderDetail;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public double getTotalTaxes() {
		return totalTaxes;
	}

	public boolean isDiscountApplied() {
		return discountApplied;
	}

	public boolean isPayed() {
		return payed;
	}

	public void setDiscountApplied(boolean discountApplied) {
		this.discountApplied = discountApplied;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setOrderDetail(List<OrderDetail> orderDetail) {
		this.orderDetail = orderDetail;
	}

	public void setPayed(boolean payed) {
		this.payed = payed;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public void setTotalTaxes(double totalTaxes) {
		this.totalTaxes = totalTaxes;
	}

}
