package com.uxs.orange.restaurant.schedulers;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.uxs.orange.restaurant.entities.Meal;
import com.uxs.orange.restaurant.services.MealService;

@Component
public class AvailabilityScheduler {

	private static Logger logger = LogManager.getLogger(AvailabilityScheduler.class);

	@Autowired
	private MealService mealService;

	@Value("${meal.refillQuantity}")
	private Integer refillQuantity;

	@Scheduled(fixedRate = 1000 * 60)
	public void refillStock() {

		logger.info("Checking stock...");

		List<Meal> unvailableMeals = mealService.findAllUnvailable();

		if (!unvailableMeals.isEmpty()) {
			logger.warn("Ordering...");
			for (Meal meal : unvailableMeals) {
				mealService.refill(meal, refillQuantity);
			}
		}
	}

}
