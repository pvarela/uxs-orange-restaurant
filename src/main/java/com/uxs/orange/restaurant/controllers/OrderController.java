package com.uxs.orange.restaurant.controllers;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uxs.orange.restaurant.dtos.OrderDTO;
import com.uxs.orange.restaurant.dtos.OrderDetailDTO;
import com.uxs.orange.restaurant.services.OrderService;

@RestController
@RequestMapping("/orders")
public class OrderController {

	private static Logger logger = LogManager.getLogger(OrderController.class);

	@Autowired
	private OrderService orderService;

	@PostMapping
	public ResponseEntity<OrderDTO> createOrder(@RequestBody List<OrderDetailDTO> orderDetails) {
		OrderDTO order = orderService.createOrder(orderDetails);
		logger.info("A new order has been created");
		if (order == null) {
			return ResponseEntity.badRequest().build();
		}
		return ResponseEntity.ok(order);
	}

	@GetMapping
	public List<OrderDTO> getAll() {
		logger.info("All orders registered has been requested");
		return orderService.findAll();
	}

	@PutMapping("/{id}")
	public ResponseEntity<OrderDTO> payOrder(@PathVariable Long id) {
		OrderDTO order = orderService.payOrder(id);

		if (order == null) {
			logger.info("The order with id " + id + " has not been found");
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(order);

	}
}
