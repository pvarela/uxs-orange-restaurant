package com.uxs.orange.restaurant.controllers;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.uxs.orange.restaurant.dtos.MealDTO;
import com.uxs.orange.restaurant.services.MealService;

@RestController
@RequestMapping(value = "/meals")
public class MealController {

	private static Logger logger = LogManager.getLogger(MealController.class);

	@Autowired
	private MealService mealService;

	@PostMapping
	public ResponseEntity<MealDTO> createMeal(@RequestBody MealDTO meal) {
		logger.info("Meal created");
		MealDTO mealCreated = mealService.createMeal(meal);

		if (mealCreated == null) {
			return ResponseEntity.badRequest().body(new MealDTO());
		}
		return ResponseEntity.ok(mealCreated);
	}

	@GetMapping
	public ResponseEntity<List<MealDTO>> getAll() {
		logger.info("All meals requested");
		return ResponseEntity.ok(mealService.findAll());
	}

	@GetMapping("/availables")
	public ResponseEntity<List<MealDTO>> getAllAvailables() {
		logger.info("Available meals requested");
		return ResponseEntity.ok(mealService.findAllAvailable());
	}

	@GetMapping(params = "category")
	public ResponseEntity<List<MealDTO>> getAllByCategory(@RequestParam("category") String category) {
		logger.info("Requested meals from category " + category);
		return ResponseEntity.ok(mealService.findAllByCategory(category));
	}

}
