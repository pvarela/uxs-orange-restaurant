package com.uxs.orange.restaurant.controllers;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.uxs.orange.restaurant.dtos.MealDTO;
import com.uxs.orange.restaurant.services.MealService;

@RunWith(MockitoJUnitRunner.class)
public class MealControllerTest {

	@InjectMocks
	private MealController mealController;

	@Mock
	private MealService mealService;

	@Test
	public void createMealKOTest() {
		when(mealService.createMeal(any())).thenReturn(null);
		ResponseEntity<MealDTO> responseEntity = mealController.createMeal(any());
		assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
	}

	@Test
	public void createMealOKTest() {
		when(mealService.createMeal(any())).thenReturn(new MealDTO());
		ResponseEntity<MealDTO> responseEntity = mealController.createMeal(any());
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
	}

	@Test
	public void getAllAvailablesTest() {
		when(mealService.findAllAvailable()).thenReturn(new ArrayList<MealDTO>());
		ResponseEntity<List<MealDTO>> responseEntity = mealController.getAllAvailables();
		assertEquals(0, responseEntity.getBody().size());
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
	}

	@Test
	public void getAllByCategoryTest() {

	}

	@Test
	public void getAllTest() {

	}

}
