package com.uxs.orange.restaurant.services;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.uxs.orange.restaurant.dtos.MealDTO;
import com.uxs.orange.restaurant.entities.Meal;
import com.uxs.orange.restaurant.enums.MealCategory;
import com.uxs.orange.restaurant.repositories.MealRepository;

import ma.glasnost.orika.MapperFacade;

@RunWith(MockitoJUnitRunner.class)
public class MealServiceTest {

	private Meal meal;

	private MealDTO mealCreatedDTO;

	@InjectMocks
	private MealService mealService;

	@Mock
	private MealRepository mealRepository;

	@Mock
	private MapperFacade orika;

	@Before
	public void setUp() {
		this.meal = new Meal();
		this.meal.setName("Cheeseburguer");
		this.meal.setCategory(MealCategory.SECOND_COURSE);
		this.meal.setPrice(10.0);
		this.meal.setTaxes(0.1);
		this.meal.setQuantity(100);

		this.mealCreatedDTO = new MealDTO();
		this.mealCreatedDTO.setId(1L);
		this.mealCreatedDTO.setName("Cheeseburguer");
		this.mealCreatedDTO.setCategory(MealCategory.SECOND_COURSE);
		this.mealCreatedDTO.setPrice(10.0);
		this.mealCreatedDTO.setTaxes(0.1);
		this.mealCreatedDTO.setQuantity(100);
	}

	@Test
	public void createMealFail() {

	}

	@Test
	public void createMealSuccess() {
		MealDTO mealDTO = new MealDTO();
		mealDTO.setName("Cheeseburguer");
		mealDTO.setCategory(MealCategory.SECOND_COURSE);
		mealDTO.setPrice(10.0);
		mealDTO.setTaxes(0.1);
		mealDTO.setQuantity(100);
		when(orika.map(mealDTO, Meal.class)).thenReturn(this.meal);

		MealDTO expectedMeal = mealService.createMeal(mealDTO);
		when(orika.map(expectedMeal, MealDTO.class)).thenReturn(this.mealCreatedDTO);

		System.out.println(expectedMeal);
		System.out.println(this.mealCreatedDTO);
		assertEquals(expectedMeal.getId(), this.mealCreatedDTO.getId());
	}

	@Test
	public void discountQuantityFail() {

	}

	@Test
	public void discountQuantitySuccess() {

	}

	@Test
	public void findAllAvailableSuccess() {

	}

	@Test
	public void findAllByCategorySuccess() {

	}

	@Test
	public void findAllSuccess() {

	}

	@Test
	public void findAllUnvailableSuccess() {

	}

	@Test
	public void findByIdFail() {

	}

	@Test
	public void findByIdSuccess() {

	}

	@Test
	public void refillSuccess() {

	}

}
